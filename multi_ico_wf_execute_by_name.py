# modified https://github.com/rtortori/ico-python-examples with minor changes

import credentials
from intersight.api import workflow_api
from intersight.api import organization_api
from intersight.rest import ApiException

from intersight.model.organization_organization import OrganizationOrganization

from intersight.model.workflow_workflow_info import WorkflowWorkflowInfo
from intersight.model.mo_base_mo_relationship import MoBaseMoRelationship
from intersight.model.workflow_workflow_definition_relationship import WorkflowWorkflowDefinitionRelationship


# Get Workflow Moid based on workflow name

def get_workflow_definitions(api_client):
    """ Gets the list of workflows """
    api_instance = workflow_api.WorkflowApi(api_client)

    try:
        query_filter = "Label eq '{0}'".format(workflow_name)
        api_response = api_instance.get_workflow_workflow_definition_list(filter=query_filter,top=0,_check_return_type=False)
        return api_response.results[0]['Moid']
    except ApiException as e:
        print("Exception when calling WorkflowApi->get_workflow_workflow_definition_list: %s\n" % e)


# Get Workflow Moid based on Organization

def get_organization_definitions(api_client):
    """ Gets the list of workflows """
    api_instance = organization_api.OrganizationApi(api_client)
    try:
        query_filter = "Name eq '{0}'".format(org_name)
        api_response = api_instance.get_organization_organization_list(filter=query_filter,top=0,_check_return_type=False)
        # api_response = api_instance.get_organization_organization_list(select='name')
        return api_response.results[0]['Moid']
    except ApiException as e:
        print("Exception when calling OrganizationApi->get_organization_oraganization_list: %s\n" % e)




# Workflow Definition

def execute_workflow(workflow, api_client):
    """ Execute a Workflow """
    api_instance = workflow_api.WorkflowApi(api_client)

    try:
        api_response = api_instance.create_workflow_workflow_info(workflow,_check_return_type=False)
        return api_response
    
    except ApiException as e:
        print("Exception when calling WorkflowApi->create_workflow_workflow_info: %s\n" % e)

# Variables. Change accordingly

# workflow list as you want to exectute
wk_list = [
    {
        "workflow_name": 'SSHAPI',
        "org_name": 'default',
        "workflow_inputs": {
            "ssh": {
            "Command": "ls",
            "CommandType": "NonInteractiveCmd"
            }
        }
    },
    {
        "workflow_name": 'SSHAPI',
        "org_name": 'default',
        "workflow_inputs": {
            "ssh": {
            "Command": "ls /",
            "CommandType": "NonInteractiveCmd"
            }
        }
    },
]

# Authenticate
api_client = credentials.config_credentials()

for wk in wk_list:
   workflow_name = wk['workflow_name']
   org_name = wk['org_name']
   workflow_inputs = wk['workflow_inputs']
   workflow_moid = get_workflow_definitions(api_client)
   org_moid = get_organization_definitions(api_client)

   #### Workflow Payload ###
   ass_obj = MoBaseMoRelationship(
      moid=org_moid,
      object_type="organization.Organization",
      class_id="mo.MoRef"
   )
   workflow_def = WorkflowWorkflowDefinitionRelationship(
      moid = workflow_moid,
      object_type = "workflow.WorkflowDefinition",
      class_id="mo.MoRef"
   )
   workflow = WorkflowWorkflowInfo(
      name=workflow_name,
      associated_object=ass_obj,
      action="Start",
      input = workflow_inputs,
      workflow_definition=workflow_def
   ) 
   
   # Execute the Workflow
   exec = execute_workflow(workflow,api_client)
   print(exec)
   print()
   print('Execution Moid: {}'.format(exec['moid']))
   print('Executed by: {}'.format(exec['email']))