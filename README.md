# Managed Intersight workflow

This repository contains sample script to invoke intersight workflow .

DISCLAIMER: This is NOT an official Cisco repository and comes with NO WARRANTY AND/OR SUPPORT
Please check LICENSE-CISCO for additional details

## Getting started
It needs the operations to invoke scripts.

1. Obtaining API Key in Intersight
2. Installing intersight python sdk 
3. Modified Input Variables to invoke your custom workflow.


## How to invoke the script 
You will find 3 scripts in the repository for invoking Intersight Cloud Orchestrator Workflow.

1. ```ico_wf_execute_by_name.py``` - Modified variable and invoke ICO workflow based on the workflow name.
2. ```multi_ico_wf_execute_by_name.py``` - Modified multiple variable and invoke ICO workflow with variable loop based on the workflow name.
3. ```ico_wf_execute_by_excel``` - Provided excel file and then invoke workflow with variable loop. It is not embdded variable in the scripts.


### Obtaining API Key in Intersight
It needs to obtain API Key from Intersight. API_KEY_ID and Secret file are required to invoke the script.

```console
$ export INTERSIGHT_API_PRIVATE_KEY=/Path/To/secret.txt
$ export INTERSIGHT_API_KEY_ID=xxxxxxxxxxxxxxxxxxxxxxxxx
```
### Installing intersight python sdk 
For intersight python sdk, you need to install intersight pip package. It is simply to run as follows.

```console
$ pip install intersight
```
We need some addtional python package to invoke ```ico_wf_execute_by_excel```.
```consoel
$ pip install pandas
$ pip install openpyxl
$ pip install xlrd
```

### Modified input variables to invoke you custom workflow

It recommend to check workflow inputs with Chrome Developer Tool before invoking the custom script. And edit workflow_inputs and Organizatin ID in ico_wf_execute_by_name.py.

``` 
workflow_inputs = {
    "ssh": {
    "Command": "ls",
    "CommandType": "NonInteractiveCmd"
    }
}
```

### Input Parameters
Please fill out the script in the embedding or excel file as you prefer to invoke the script.

1. Workflow Name - ICO workflow name
2. Organization Name - Need to specify the organization name under what organiation.
3. Workflow Inputs - Need to specify the workflow input variable as needed.
